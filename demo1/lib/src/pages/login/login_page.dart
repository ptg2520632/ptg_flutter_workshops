// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:demo1/src/constants/asset.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _usernameController.text = "admin";
    _passwordController.text = "1234";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 70),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            // Header
            SizedBox(height: 100, child: Image.asset(Asset.logoImage)),
            // Form
            _buildForm()
          ],
        ),
      ),
    );
  }

  _buildForm() {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Card(
        elevation: 7,
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Username
              TextField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'codemobiles@gmail.com',
                    labelText: 'Username',
                    icon: Icon(Icons.email),
                  )),

              // Password
              TextField(
                  controller: _passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: '',
                    labelText: 'Password',
                    icon: Icon(Icons.lock),
                  )),

              // Login Button
              ElevatedButton(onPressed: _login, child: Text("Login")),

              //Register
              OutlinedButton(onPressed: _register, child: Text("Register")),
            ],
          ),
        ),
      ),
    );
  }

  void _login() {
    Navigator.pushNamed(context, "home");
  }

  void _register() {
    Navigator.pushNamed(context, "register");
  }
}
