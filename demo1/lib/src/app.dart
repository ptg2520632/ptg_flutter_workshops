import 'package:demo1/src/pages/home/home_page.dart';
import 'package:demo1/src/pages/login/login_page.dart';
import 'package:demo1/src/pages/register/register_page.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "",
      routes: {
        "login": (context) => LoginPage(),
        "home": (context) => HomePage(),
        "register": (context) => RegisterPage(),
      },
      home: LoginPage(),
    );
  }
}
