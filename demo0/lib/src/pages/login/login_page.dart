import 'package:demo0/src/pages/login/widgets/ptg_text_box.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Login")),
      body: Container(
        padding: EdgeInsets.only(top: 20),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(
              height: 70,
              child: Image.asset("assets/images/logo.png"),
            ),
            PTGTextBox(
              color: Colors.red,
              text: "Big",
            ),
            PTGTextBox(
              color: Colors.yellow,
              text: "Oat",
            ),
            PTGTextBox(
              color: Colors.blue,
              text: "Few",
            ),
          ],
        ),
      ),
    );
  }
}
