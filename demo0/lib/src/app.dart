import 'package:flutter/material.dart';
import 'pages/login/login_page.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Demo0",
      home: LoginPage(),
    );
  }
}
